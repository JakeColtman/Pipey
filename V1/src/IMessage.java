import java.util.HashMap;

/**
 * Created by jacoltman on 15/09/2016.
 */
public interface IMessage {
    String get_body();

    HashMap get_headers();
}
