import java.util.ArrayList;
import java.util.List;

public class MessageQueue {

    private String name;
    private List<Message> store;

    MessageQueue(String name){
        this.name = name;
        this.store = new ArrayList<>();
    }

    public Boolean offer (Message message){
        final boolean add = this.store.add(message);
        return Boolean.TRUE;
    }

    public Message remove (){
        return this.store.remove(0);

    }

}
