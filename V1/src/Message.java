import java.util.HashMap;

public class Message implements IMessage {

    private HashMap headers;
    private String body;

    Message(String body){
        this.body = body;
        this.headers = new HashMap();
    }

    public String get_body(){
        return this.body;
    }

    public HashMap get_headers(){
        return this.headers;
    }

}
